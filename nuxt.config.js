export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'fbr-nuxt',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      // {
      //  src :"https://cdn.jsdelivr.net/npm/body-scroll-lock/lib/bodyScrollLock.min.jsx",
      //  type: "text/javascript"
      // },
      // {
      //   src: "https://unpkg.com/vue-cool-lightbox",
      //   type: "text/javascript"
      //  }
      // {
      //   src: "https://code.jquery.com/jquery-3.3.1.slim.min.js",
      //   type: "text/javascript"
      // },
      // {
      //   src: "https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js",
      //   type: "text/javascript"
      // },
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/mobile.css',
    // '~/swiper/dist/css/swiper.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    //['~/plugins/flickity.pkgd.min.js']
    // { src: '~/plugins/flickity/flickity.pkgd.min.js' },
    // { src: '~/plugins/jquery-3.3.1.slim.min.js', ssr: false },
    { src: '~/plugins/flickity.js', ssr: false },
    //{ src: '~/plugins/cool-lightbox.js', ssr: false },
    //{ src: '~/plugins/vue-lightbox-plugin.js', ssr: false },
    { src: '~/plugins/lightGallery.client.js', ssr: false},
    { src: './plugins/vue-concise-slider.js', mode: 'client' },
    // { src: '~/plugins/vue-awesome-swiper.js', ssr: false },
    // { src: '@/plugins/nuxt-swiper-plugin.js', mode: 'client' },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios'
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
